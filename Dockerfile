FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install vlc -y

ADD video.mp4 /
RUN chmod +r /video.mp4

ADD start-vlc.sh /start-vlc.sh
RUN chmod +x /start-vlc.sh
ADD start-container.sh /start-container.sh
RUN chmod +x /start-container.sh

EXPOSE 8000
ENTRYPOINT /start-container.sh
