The contents of this folder can be used to build a Docker image that provides a
VLC server able to be used in the Adaptive Quality of Experience 
proof-of-concept of the EU FP7 ONTIC project.

By default, this image exposes the port 8000. If you want to change said port:
* in ```Dockerfile```, change ```EXPOSE 8000``` to the desired port.
* in ```start-vlc.sh```, change the port in ```dst=0.0.0.0:8000``` to the same 
port provided in the previous step.

The image is built to issue a video named ```video.avi```. If you want to change 
the name of the video file to be issued, in ```start-vlc.sh```, change 
```vlc -v /video.mp4```  to the new file name. 
**You should not change the folder in the file path**, only the file name.

The AQoE VLS image is created by downloading all the contents of this folder
and executing the following commands in the folder where all the contents have
been downloaded. 

The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/aqoe-vls/)
and Its intended to be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
